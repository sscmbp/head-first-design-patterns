using System;

namespace StrategyPatternNS.QuackNS {
    public class Squeak: IQuackBehavior {
        public void Quack(){
            Console.WriteLine("Squeak! Squeak!");
        }
    }
}
