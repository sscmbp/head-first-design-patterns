namespace StrategyPatternNS {
    public interface IQuackBehavior {
        void Quack();
    }
}