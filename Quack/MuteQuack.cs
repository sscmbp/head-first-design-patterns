using System;

namespace StrategyPatternNS.QuackNS {
    public class MuteQuack: IQuackBehavior {
        public void Quack(){
            Console.WriteLine("<< Silence!! >>");
        }
    }
}
