using System;

namespace StrategyPatternNS.QuackNS {
    public class Quack: IQuackBehavior {
        // public void Quack(){
        //     Console.WriteLine("Quack! Quack!");
        // }
        void IQuackBehavior.Quack(){
            Console.WriteLine("Quack! Quack!");
        }
    }
}