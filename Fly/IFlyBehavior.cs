namespace StrategyPatternNS{
    public interface IFlyBehavior {
        void fly();
    }
}