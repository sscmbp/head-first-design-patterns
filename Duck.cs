using System;

namespace StrategyPatternNS {
    public abstract class Duck {

        public IFlyBehavior flyBehavior;
        public IQuackBehavior quackBehavior;

        public Duck() {
            Console.WriteLine("Duck constructor instantiated!");
        }

        public abstract void display();

        public void performFly(){
            flyBehavior.fly();
        }

        public void performQuakc(){
            quackBehavior.Quack();
        }

        public void swim(){
            Console.WriteLine("Swim: All ducks float, even decoys!");
        }
    }
}
