using System;

namespace StrategyPatternNS
{
    public class MallardDuck: Duck {

        public MallardDuck(){
            
            Console.WriteLine("Mallard Duck instantiated!");
            quackBehavior = new Quack();
            flyBehavior = new FlyWithWings();
        }

        public override void display(){
            System.Console.WriteLine("I am a real Mallard duck!");
        }
    }
}